
// basic interest calculator program.
// it calculates simple interest of the given amount.
// it is an accrued amount that includes principal plus interest.
package interestcalculator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Scanner;

public class InterestCalculator 
{    
    // main method begins execution of Java application.
    public static void main(String[] args)
    {
        // welcome the user to the program.
        JOptionPane.showMessageDialog(null, "Welcome to Interest Calculator", "Interest Calculator", JOptionPane.PLAIN_MESSAGE);
        InterestCalculator myIntCalcProg = new InterestCalculator();
        myIntCalcProg.UserInput();
        myIntCalcProg.Calculator();
        myIntCalcProg.Output();   
    } // end main method.
    
    // variable declaration.
    // A = total accrued amount (principal + interest)
    private static float accruedAmount;    
    // P = principal amount
    private static float principal;
    // R = r * 100
    private static float percentageRate;    
    // r = rate of interest per year in decimal; r = R/100
    private static float rate;    
    // t = time period involved in years
    private static int time;
    
    // method to collect user input data.
    public void UserInput()
    {
        principal = Float.parseFloat(JOptionPane.showInputDialog(null, "Please input the principal amount", "Interest Calculator", JOptionPane.QUESTION_MESSAGE));
        percentageRate = Float.parseFloat(JOptionPane.showInputDialog(null, "Please input the intrest rate in percentage %", "Interest Calculator", JOptionPane.QUESTION_MESSAGE));
        time = Integer.parseInt(JOptionPane.showInputDialog(null, "Please input the time period involved in years", "Interest Calculator", JOptionPane.QUESTION_MESSAGE));
    } // end UserInput method.
    
    // method to calcualte the total accrued amount (principal + interest).
    public void Calculator()
    {
        rate = percentageRate / 100;
        accruedAmount = (principal * (1 + ( rate * time )));    
    } // end Calculator method.
    
    // method to display the result to the user.
    public void Output()
    {
        JOptionPane.showMessageDialog(null, "The total accrued amount (principal + interest) is: " + accruedAmount, "Interest Calculator", JOptionPane.INFORMATION_MESSAGE);
    } // end Output method.
    
} // end class InterestCalculator.
